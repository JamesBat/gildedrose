# Make de Gilded Rose KATA with Java
Pour pouvoir mettre en place mon Kata j'aurais utilisé les principes de :
- TDD
- Simple Design
- Refactoring

La documentation de spécification Gilded Rose qui reprend les principes à respecter est disponible sur [Gilded Rose](https://github.com/emilybache/GildedRose-Refactoring-Kata/blob/main/GildedRoseRequirements_fr.md)

## Disponibilité du code
Le code souce est disponible sur le dépot Gitlab : [Gilded Rose Exercises](https://gitlab.com/JamesBat/gildedrose) 

## Structure des branches
Le Repository comprend 02 branches à savoir **main** & **development**
Le Kata a été effectué sur la branche development, la branche main à été mis a jour après des Merge Request effectué depuis development.

## Execution
Vous pouvez executer les test en lancant le fichier test : `GildedRoseTest.java` ( /gilded-rose-kata/src/test/java/com/gildedrose/GildedRoseTest.java ) qui contient l'ensemble des tests unitaires de l'application

Pour Executer l'application vous pouvez lancer le ficher : `TexttestFixture.java` ( /gilded-rose-kata/src/test/java/com/gildedrose/TexttestFixture.java ) qui contient la fonction main du programme. 

## Deroule du processus d'implémentation
Step 01 : Mise en place des test unitaires
Step 02 : Refactor de la fonction `updateQuality()` de la classe GildedRose succésivement en intégrant les cas des classes (`StandardItem.java`,`AgedBrie.java`,`BackstagePasses.java`,`Sulfuras.java`,`Conjured.java`).
Des classes utilitaires pour pouvoir traiter le cas des produits Conjured sans alerter le Gobelin auront été necéssaires (`ItemFactory.java`,`CustomisedItem.java`,`StandardItem.java`)

#### Thank You
J'espère que vous passerez un bon moment en decouvrant mon Gilded Rose Kata, n'hésitez pas à me revenir pour toutes questions. Merci !!!