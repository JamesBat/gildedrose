package com.gildedrose;

import java.util.List;
import java.util.stream.Collectors;

import com.gildedrose.item.CustomisedItem;
import com.gildedrose.item.Item;
import com.gildedrose.item.ItemFactory;

class GildedRose {
    List<Item> items;

    public GildedRose(List<Item> items) {
        this.items = items;
    }
   
    public void  updateQuality() {
    	items = items.stream().map(item -> {
    			customisedItem(item).updateQuality();
    			return item;
    		}).collect(Collectors.toList());
    }
    
    private CustomisedItem customisedItem(Item item) {
        return new ItemFactory(item).customiseItem(item);
    }
}