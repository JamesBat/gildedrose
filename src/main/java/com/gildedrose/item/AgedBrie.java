package com.gildedrose.item;

public class AgedBrie implements CustomisedItem {

	private final Item item;

    public AgedBrie(Item item) {
        this.item = item;
    }
    
    private int decreasingValue() {
    	if(item.quality == 50) return 0;
    	return (item.sellIn >= 0)?1:2;
    }

	@Override
    public void updateQuality() {
		item.quality = item.quality + decreasingValue();
        item.sellIn--;
    }
}
