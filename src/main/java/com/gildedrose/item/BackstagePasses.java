package com.gildedrose.item;

public class BackstagePasses implements CustomisedItem {

	private final Item item;

    public BackstagePasses(Item item) {
        this.item = item;
    }
    
    private int decreasingValue() {
    	int val = 1;
    	if(item.sellIn>=5 && item.sellIn <=10) val = 2;
    	if(item.sellIn>=1 && item.sellIn <=4) val = 3;
    	if(item.sellIn < 0) val = -item.quality;
    	return val;
    }

	@Override
    public void updateQuality() {
		item.quality = item.quality + decreasingValue() <= 50?item.quality + decreasingValue():50;
        item.sellIn--;
    }

}
