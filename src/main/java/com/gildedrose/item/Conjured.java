package com.gildedrose.item;

public class Conjured implements CustomisedItem {

	private final Item item;

    public Conjured(Item item) {
        this.item = item;
    }
    
    private int decreasingValue() {
    	return 2;
    }
    
	@Override
	public void updateQuality() {
		item.quality = item.quality - decreasingValue();
        item.sellIn--;
	}
}