package com.gildedrose.item;

import java.util.HashMap;
import java.util.Map;

public class ItemFactory {

	private final static Map<String, CustomisedItem> ITEM_TYPES_LIST = new HashMap<>();
	private static final String AGED_BRIE = "Aged Brie";
	private static final String BACKSTAGE_PASSES = "Backstage passes to a TAFKAL80ETC concert";
	private static final String SULFURAS = "Sulfuras, Hand of Ragnaros";
	private static final String CONJURED = "Conjured";
	
	public ItemFactory(Item item) {
		ITEM_TYPES_LIST.put(AGED_BRIE, new AgedBrie(item));
		ITEM_TYPES_LIST.put(BACKSTAGE_PASSES, new BackstagePasses(item));
		ITEM_TYPES_LIST.put(SULFURAS, new Sulfuras());
		ITEM_TYPES_LIST.put(CONJURED, new Conjured(item));
	}

	public CustomisedItem customiseItem(Item item) {
		if(isStandardItem(item)) return new StandardItem(item);
		return ITEM_TYPES_LIST.get(item.name);
	}

	private boolean isStandardItem(Item item) {
        return !ITEM_TYPES_LIST.keySet().contains(item.name);
    }
}