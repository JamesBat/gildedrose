package com.gildedrose.item;

public class StandardItem implements CustomisedItem{
	
	private final Item item;

    public StandardItem(Item item) {
        this.item = item;
    }
    
    private int decreasingValue() {
    	if(item.quality == 0) return 0;
    	return (item.sellIn >= 0)?1:2;
    }

	@Override
    public void updateQuality() {
		item.quality = item.quality - decreasingValue();
        item.sellIn--;
    }
}
