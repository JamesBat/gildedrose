package com.gildedrose;

import java.util.ArrayList;
import java.util.List;

import com.gildedrose.item.Item;

public class TexttestFixture {
    public static void main(String[] args) {
    	List<Item> items = new ArrayList<Item>();
    	items.add(new Item("+5 Dexterity Vest", 10, 0));
    	items.add(new Item("Aged Brie", -1, 0));
    	items.add(new Item("Elixir of the Mongoose", 5, 7));
    	items.add(new Item("Sulfuras, Hand of Ragnaros", 0, 80));
    	items.add(new Item("Sulfuras, Hand of Ragnaros", -2, 80));
    	items.add(new Item("Backstage passes to a TAFKAL80ETC concert", 15, 20));
    	items.add(new Item("Backstage passes to a TAFKAL80ETC concert", -1, 49));
    	items.add(new Item("Backstage passes to a TAFKAL80ETC concert", 5, 49));
    	items.add(new Item("Conjured", 3, 6));

        GildedRose app = new GildedRose(items);

        int days = 2;
        if (args.length > 0) {
            days = Integer.parseInt(args[0]) + 1;
        }

        for (int i = 0; i < days; i++) {
            for (Item item : items) {
                System.out.println(item);
            }
            System.out.println();
            app.updateQuality();
        }
    }
}